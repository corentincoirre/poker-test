import unittest
from class_poker import PokerHand

class PokerHandTest(unittest.TestCase):
    """Test class for all methods of PokerHand"""

    def setUp(self):
        self.hand1 = PokerHand("AH TH QH KH JH")
        self.hand2 = PokerHand("AH AS AD TH TC")
        self.hand3 = PokerHand("QC 5S 6S 4C 9D")

    def test_init(self):
        """Test if initialization correctly works"""

        self.assertEqual(self.hand1.hand,["AH", "TH", "QH", "KH", "JH"])
        self.assertRaises(TypeError,PokerHand,5)
        self.assertRaises(ValueError,PokerHand,"AH TH QH KH JHZ")

    def test_card_aggregation(self):

        """Test method card_agregation"""
        
        agg = {"A" : 1,"T": 1, "Q" : 1, "K" : 1, "J" : 1}
        self.assertEqual(self.hand1.card_aggregation(),agg)

    def test_is_straight(self):

        """Test method is_straight"""
        
        self.assertTrue(self.hand1.is_straight())
        self.assertFalse(self.hand2.is_straight())
        self.assertFalse(self.hand3.is_straight())       

    def test_is_flush(self):

        """Test method is_flush"""
        
        self.assertTrue(self.hand1.is_flush())
        self.assertFalse(self.hand2.is_flush())

    def test_hand_value(self):

        """Test method hand_value"""
        
        self.assertEqual(self.hand1.hand_value(),9)

    def test_sorted_list_value(self):

        """Test method sorted_list_value"""
        
        self.assertEqual(self.hand2.sorted_list_value(),[(3,13),(2,9)])

    def test_compare_with(self):

        """Test method compare with"""
        
        self.assertEqual(self.hand1.compare_with(self.hand2),1)

        self.assertRaises(ValueError,self.hand3.compare_with,self.hand1)


unittest.main()