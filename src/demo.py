from class_poker import PokerHand

hand1 = PokerHand("AH TS 5H QH KH")

hand2 = PokerHand("2H TS 5H QH KH")

result = hand1.compare_with(hand2)

print(result)