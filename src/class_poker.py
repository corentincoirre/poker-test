class PokerHand:
    """Class that dertermines the rank of a hand
    of poker and compare two hands. 
    The class is initialized using a dictionary
    that give a numeric value for each kind of card.
    """
    card_value = {"2" : 1,"3": 2,"4": 3,
        "5": 4,"6": 5,"7": 6,"8": 7,"9": 8,
        "T": 9,"J": 10,"Q": 11,"K": 12,"A": 13}

    def __init__(self, hand):
        """
        Test if the argument is correctly formated.
        Initialize attributes hand, cards_value and card_value_egality.
        """
        if not isinstance(hand,str):
            raise TypeError("must be str, not " + str(type(hand)))

        if len(hand.split()) != 5:
            raise ValueError("wrong format of string")

        list_card = []

        for card in hand.split():
            if len(card) != 2:
                raise ValueError("wrong format of string")
            elif card[0] not in PokerHand.card_value.keys():
                raise ValueError("wrong format of string")
            elif card[1] not in ["H", "S", "D", "C"]:
                raise ValueError("wrong format of string")
            if card in list_card:
                raise ValueError("duplicate card")
            list_card.append(card)

        self.hand = hand.split()
        self.cards_value = sorted(list(map(lambda x: PokerHand.card_value[x[0]],self.hand)))
        self.card_value_egality = {"2" : 1,"3": 2,"4": 3,
            "5": 4,"6": 5,"7": 6,"8": 7,"9": 8,
            "T": 9,"J": 10,"Q": 11,"K": 12,"A": 13}

    def card_aggregation(self):
        """Count the number of cards by their kinds.
        Return a dictionary with the kind of card
        as key and the number as value."""
        rep = {}
        for card in self.hand:
            rep[card[0]] = rep.get(card[0],0) + 1

        return rep

    def is_straight(self):
        """Test if there are 5 consecutive cards in the hand.
        Aces can count as either a high or a low card.
        Function return a boolean.
        Function also modify the value of ace if there is a low straight"""
        if self.cards_value[4] == 13:
            self.cards_value[4] = 0
            self.cards_value.sort()
            if all(map(lambda x : self.cards_value[x] + 1 == self.cards_value[x+1],range(0,4))):
                self.card_value_egality["A"] = 0
                return True
            else:
                self.cards_value[0] = 13
                self.cards_value.sort()
                return all(map(lambda x : self.cards_value[x] + 1 == self.cards_value[x+1],range(0,4)))
        else:
            return all(map(lambda x : self.cards_value[x] + 1 == self.cards_value[x+1],range(0,4)))

    def is_flush(self):
        """Test if there are 5 cards of the same suit.
        Function return a boolean."""
        return all(map(lambda x: self.hand[x][1] == self.hand[0][1],range(1,5)))

    def hand_value(self):
        """Identify the type of the hand.
        Return a value between 1 and 9 that 
        correspond to the rank of the hand type."""
        agg = self.card_aggregation()
        if sorted(agg.values()) == [1,1,1,2]:
            return 2
        elif sorted(agg.values()) == [1,2,2]:
            return 3
        elif sorted(agg.values()) == [1,1,3]:
            return 4
        elif sorted(agg.values()) == [2,3]:
            return 7
        elif sorted(agg.values()) == [1,4]:
            return 8
        elif sorted(agg.values()) == [1,1,1,1,1]:

            if self.is_flush() and self.is_straight():
                return 9
            elif self.is_straight():
                return 5
            elif self.is_flush():
                return 6
            else:
                return 1

    def sorted_list_value(self):
        """
        The function return a list of tuples (number of card,card value). 
        Tuples are sorted first by the number of cards of a kind and then 
        the value of the card(s).
        """
        agg = self.card_aggregation()
        return sorted(list(map(lambda x: (agg[x],self.card_value_egality[x]),agg.keys())),reverse=True)


    def compare_with(self,pokerhand):
        """
        Test if there is at least 1 card in common in both hand.
        Compare the hand of this object with 
        a hand of an other PokerHand object.
        First it compare the hand type.
        Then it compare the value of cards if needed.
        It return an integer:
        1 for win, 2 for loss and 3 for tie
        """
        if all(map(lambda x: x not in pokerhand.hand, self.hand)):
            raise ValueError("there is 0 card in common")

        if self.hand_value() > pokerhand.hand_value():
            return 1
        elif self.hand_value() < pokerhand.hand_value():
            return 2
        else:
            for val1, val2 in zip(self.sorted_list_value(),pokerhand.sorted_list_value()):
                if val1 > val2:
                    return 1
                elif val1 < val2:
                    return 2
            return 3