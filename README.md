# Poker test

## Run Démo

The class_test.py file provide a class PokerHand.
This class needs a string to describe the 5 cards of the hand, in order to initialize a PokerHand object.
The string needs to be formatted as describe in the instructions.
PokerHand provide a method "compare_with" which allows to compare the hand with the hand of an other PokerHand object.

To run the demo you just need to run the file demo.py.
You can change the values of hand1 and hand2 to test other combinations.
It returns the result of the function "compare_with" as describe in the instructions for hand1 object.

You can check if tests display "OK" by running the file class_test.py.

## Time needed

It took me 6 hours to design this algorithm:
- 1 hour to learn all rules of poker and its exceptions (such as aces that can be either a high or a low card).
- 1 hour to design the algorithm and split it into several functions.
- 2 hours to write the functions and the tests.
- 1 hour to comment the code and make it readable.
- 1 hour to test the program and solve issues.
